import React from "react";

class ConferenceForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: []
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            this.setState({locations: data.locations});
            // const selectTag = document.getElementById('location');

            // for (let location of data.locations) {
            //     console.log(location.name)
            //     const name = location.name;
            //     const id = location.id;
            //     // const id = parseInt(location.href.replace(/[^0-9\.]/g, ''), 10);
            //     let optionTag = locationOptions(id, name);
            //     selectTag.innerHTML += optionTag;
            // }
        }
    }

    handleInputChange(event) {
        const value = event.target.value;
        this.setState({[event.target.id]: value})

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: ''
            };
            this.setState(cleared);
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.name}
                            placeholder="Name" required type="text" name="name" id="name" 
                            className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.starts}
                            placeholder="Starts" required type="date" name="starts" id="starts" 
                            className="form-control" />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.ends}
                            placeholder="Ends" required type="date" name="ends" id="ends" 
                            className="form-control" />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.description}
                            placeholder="Description" required type="text" 
                            name="description" id="description" 
                            className="form-control" />
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.max_presentations}
                            placeholder="Maxiumum presentations" required type="number" 
                            name="max_presentations" id="max_presentations" 
                            className="form-control" />
                            <label htmlFor="max_presentations">Maxiumum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} 
                            value={this.state.max_attendees}
                            placeholder="Maximum attendees" required type="number" 
                            name="max_attendees" id="max_attendees" 
                            className="form-control" />
                            <label htmlFor="max_attendees">Maxiumum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleInputChange} 
                            value={this.state.location}
                            required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {this.state.locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm