import { NavLink } from "react-router-dom";

function Nav() {
    return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" 
        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
        aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="">Home</NavLink>
            </li>
            {/* <li className="nav-item">
              <a className="nav-link active" id="link-login" aria-current="page" href="login.html">Login</a>
            </li> */}
            <li className="nav-item">
              <NavLink className="nav-link active" id="link-new-location" aria-current="page" to="/locations/new">New location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" id="link-new-conference" aria-current="page" to="/conferences/new">New conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" id="link-new-presentation" aria-current="page" to="/presentations/new">New presentation</NavLink>
            </li>
          </ul>
          <NavLink className="btn btn-primary" to="/attendees/new">Attend!</NavLink>
        </div>
      </div>
    </nav>
    );
}
  
export default Nav;